const config = require('../config/config');
const mongoose = require('mongoose');

function connectMongoDB() {
    try {
        mongoose.connect(config.db, {
            useNewUrlParser: true,
            bufferMaxEntries: 0,
            // autoReconnect: true,
            socketTimeoutMS: 3000,
            maxIdleTimeMS: 180000,
            useUnifiedTopology: true,
            poolSize: 20
        })
        const db = mongoose.connection;
        db.on('error', (error) => {
            console.log(`MongoDB connecting failed: ${error}`)
        })
        db.once('open', () => {
            console.log('MongoDB connecting succeeded')
        })
        return db
    } catch (error) {
        console.log(`MongoDB connecting failed: ${error}`)
    }
}

let db = connectMongoDB();

module.exports = {
    db: db
}
